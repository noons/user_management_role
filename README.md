Ansible User Management
=======================

Minimalist user management (create/delete/linger)

Role Variables
--------------

- __user_name__: Self explanatory
- __user_comment__: User Comment AKA the displayed name
- __user_shell__: Path to shell command
- __user_state__: `absent` for remove user or `present` to create a brand new (REQUIRED)
- __user_linger__: set to `true` if you need a system user to run systemd services
- __user_sudo_nopassword__: set to `true` to get privileges without password
- __user_group__: Main group for the user
- __user_group_state__: As default is the same that __user_state__

Example Playbook
----------------

```yaml
- hosts: "{{ target }}"
  name: Check Users and Basic Packages
  become: true
  roles:
    - role: user-management-role
      vars:
        user_name: "generic"
        user_state: "present"
        user_sudo_nopassword: false
```

License
-------

[GPL-3.0-or-later](./LICENSE)

Author Information
------------------

- [@noons](https://layer8.space/@noons)
- [codeberg](https://codeberg.org/noons)
- [github](https://github.com/noonsleeper)
